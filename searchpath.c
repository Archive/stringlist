#include "StringList.h"

void
AppendPath()
{
   /* This function will append items to the 
    * end of the searchpath 
    */

}

void
UnAppendPath()
{
   /* This function will remove items from the 
    * end of the searchpath 
    */

}

void
UnPrependPath()
{
   /* This function will remove items from the 
    * beginning of the searchpath
    */

}

void
PrependPath()
{
   /* This function will insert items into the 
    * beginning of the searchpath 
    */

}

void
GetNumberOfMembersPath()
{
   /* This function will tell you how many members are
    * in the path
    */

}

void
ReturnNextItem()
{
   /* This function returns the next item in the current
    * searchpath
    */

}
