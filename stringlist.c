#include "StringList.h"

long                StringCount;

int
testForComment(char *line)
{
   EDBUG(8, "testForComment");
   if ((line[0] == '#') || (line[0] == '\n') || (line[0] == 0))
     {
	EDBUG_RETURN(1);
     }
   else
     {
	EDBUG_RETURN(0);
     }
   EDBUG_RETURN(0);
}

int
GetNextLine(char *line, FILE * ConfigFile)
{
   int                 j = 0, i = 0;
   char                line2[FILEPATH_LEN_MAX];
   char               *environment_variable = 0;
   char                env_name[FILEPATH_LEN_MAX];

   EDBUG(8, "GetNextLine");
   if (!fgets(line, FILEPATH_LEN_MAX, ConfigFile))
      EDBUG_RETURN(0);
   while (testForComment(line))
     {
	if (!fgets(line, FILEPATH_LEN_MAX, ConfigFile))
	   EDBUG_RETURN(0);
     }
   line[strlen(line) - 1] = 0;

   line2[0] = 0;

   while (line[i])
     {
	if (line[i] == '$')
	  {
	     i++;
	     if (line[i] != '$')
	       {
		  while (isalnum(line[i]) || line[i] == '_')
		     env_name[j++] = line[i++];
		  env_name[j] = 0;
		  environment_variable = getenv(env_name);
		  if (environment_variable)
		     strcat(line2, environment_variable);
		  else
		    {
		       strcat(line2, "$");
		       strcat(line2, env_name);
		    }
	       }
	  }
	else
	  {
	     char                s[2];

	     s[0] = line[i];
	     s[1] = 0;
	     strcat(line2, s);
	     ++i;
	  }
     }
   strcpy(line, line2);
   EDBUG_RETURN(1);
}

StringType         *
GenerateStringList(char *file)
{
   FILE               *definitionFile;
   char                line[FILEPATH_LEN_MAX];
   int                 definition;
   char                s[1024];
   StringType         *returnList = 0;
   long                StringCount = 0;

   EDBUG(8, "GenerateStringList");
   StringCount = 0;
   if (!file)
      definitionFile = fopen("definitions", "r");
   else
      definitionFile = fopen(file, "r");
   if (!definitionFile)
     {
	printf("Enlightenment cannot load your string definitions file:\n"
	       "%s\n"
	       "Enlightenment can probably continue to attempt to load\n"
	       "however this error may prove to be fatal.\n"
	       "If you do not require a definitions file, you should\n"
	       "create a blank file to avoid this error message in\n"
	       "the future.\n", file);
     }
   while (GetNextLine(line, definitionFile))
     {
	sscanf(line, "%d %4000s", &definition, s);
	if (!returnList)
	   returnList = Emalloc(sizeof(StringType));
	else
	   returnList = Erealloc(returnList, sizeof(StringType) * (StringCount + 1));
	returnList[StringCount].definition = definition;
	returnList[StringCount].string = Emalloc(sizeof(char) * (strlen(s) + 1));

	returnList[StringCount].string = duplicate(s);
	returnList[0].num = StringCount;
	++StringCount;
     }
   --StringCount;
   fclose(definitionFile);

   EDBUG_RETURN(returnList);
}

void
DestroyStringList(StringType * listToKill)
{
   long                i = 0;

   EDBUG(4, "DestroyStringList");
   for (i = 0; i <= listToKill[0].num; i++)
     {
	Efree(listToKill[i].string);
     }
   Efree(listToKill);
   EDBUG_RETURN_;

}

int
ReturnStringReference(StringType * StringSet, char *actionString)
{
   long                i = 0;

   EDBUG(8, "ReturnStringReference");
   for (i = 0; i <= StringSet[0].num; i++)
      if (!strcasecmp(StringSet[i].string, actionString))
	 EDBUG_RETURN(StringSet[i].definition);
   EDBUG_RETURN(0);
}

char               *
ReturnStringFromReference(StringType * StringSet, int reference)
{
   long                i = 0;

   EDBUG(8, "ReturnStringReference");

   for (i = 0; i <= StringSet[0].num; i++)
      if (StringSet[i].definition == reference)
	 EDBUG_RETURN(StringSet[i].string);
   EDBUG_RETURN(0);
}
