/*****************************************************************************/
/* StringList - the library that lives for grunt work                        */
/*****************************************************************************/
/* Copyright (C) 1998 Geoff Harrison (Mandrake) and                          */
/*                    Carsten Haitzler (The Rasterman)                       */
/* This program and utilites is free software; you can redistribute it       */
/* and/or modify it under the terms of the GNU General Public License as     */
/* published by the Free Software Foundation; either version 2 of the        */
/* License, or (at your option) any later version.                           */
/*                                                                           */
/* This software is distributed in the hope that it will be useful,          */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU         */
/* General Public License for more details.                                  */
/*                                                                           */
/* You should have received a copy of the GNU Library General Public         */
/* License along with this software; if not, write to the                    */
/* Free Software Foundation, Inc., 59 Temple Place - Suite 330,              */
/* Boston, MA 02111-1307, USA.                                               */
/*****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdarg.h>
#include <string.h>
#include <fcntl.h>
#include <dirent.h>
#include <signal.h>
#include <time.h>
#include <math.h>
#include <pwd.h>
#include <ctype.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <sys/resource.h>
#ifdef X_SUPPORT
#include <X11/Xlib.h>
#endif

typedef struct
  {
     char               *string;
     short               definition;
     long                num;
  }
StringType;

#define FILEPATH_LEN_MAX 4096
/* This turns on E's internal stack tracking system for  coarse debugging */
/* and being able to trace E for profiling/optimisation purposes (which */
/* believe it or not I'm actually doing) */

/* #define DEBUG 1 */

#ifdef DEBUG
extern int          call_level;
extern int          debug_level;

#endif

#ifdef DEBUG
#define EDBUG(l,x) \
{ \
  int i_call_level; \
  if (l<debug_level) \
    { \
      for(i_call_level=0;i_call_level<call_level;i_call_level++) \
        putchar('-'); \
      printf(" %8x %s\n",(unsigned int)time(NULL),x); \
      fflush(stdout); \
    } \
  call_level++; \
}
#else
#define EDBUG(l,x)  \
;
#endif

#ifdef DEBUG
#define EDBUG_RETURN(x)  \
{ \
/*  int i_call_level; */\
  call_level--; \
/*  for(i_call_level=0;i_call_level<call_level;i_call_level++) */\
/*    putchar('-'); */\
/*  putchar('\n'); */\
  return (x); \
}
#define EDBUG_RETURN_  \
{ \
/*  int i_call_level; */\
  call_level--; \
/*  for(i_call_level=0;i_call_level<call_level;i_call_level++) */\
/*    putchar('-'); */\
/*  putchar('\n'); */\
  return; \
}
#else
#define EDBUG_RETURN(x)  \
{ \
  return (x); \
}
#define EDBUG_RETURN_  \
{ \
  return; \
}
#endif

/* Function declaration lists */

/* memory.c functions */
void               *Emalloc(int size);
void               *Erealloc(void *ptr, int size);
void               *Ecalloc(int num, int size);
void                Efree(void *ptr);
char               *duplicate(char *s);

/* stringlist.c functions */
int                 testForComment(char *line);
int                 GetNextLine(char *line, FILE * ConfigFile);
StringType         *GenerateStringList(char *file);
void                DestroyStringList(StringType * listToKill);
int                 ReturnStringReference(StringType * StringSet, char *actionString);
char               *ReturnStringFromReference(StringType * StringSet, int reference);

/* alert.c functions */
void                Alert(char *fmt,...);
void                InitStringList(void);
void                AssignIgnoreFunction(int (*FunctionToAssign) (void *), void *params);
void                AssignRestartFunction(int (*FunctionToAssign) (void *), void *params);
void                AssignExitFunction(int (*FunctionToAssign) (void *), void *params);
void                AssignTitleText(char *text);
void                AssignIgnoreText(char *text);
void                AssignRestartText(char *text);
void                AssignExitText(char *text);

/* file.c functions */
void                md(char *s);
int                 exists(char *s);
void                mkdirs(char *s);
int                 isfile(char *s);
int                 isdir(char *s);
char              **ls(char *dir, int *num);
void                freestrlist(char **l, int num);
void                rm(char *s);
void                mv(char *s, char *ss);
void                cp(char *s, char *ss);
time_t              moddate(char *s);
int                 filesize(char *s);
void                cd(char *s);
char               *cwd(void);
int                 permissions(char *s);
int                 owner(char *s);
int                 group(char *s);
char               *username(int uid);
char               *homedir(int uid);
char               *usershell(int uid);
char               *atword(char *s, int num);
char               *atchar(char *s, char c);
void                word(char *s, int num, char *wd);
int                 canread(char *s);
int                 canwrite(char *s);
int                 canexec(char *s);
char               *fileof(char *s);
char               *fullfileof(char *s);
char               *pathtoexec(char *file);
char               *pathtofile(char *file);
