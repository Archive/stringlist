# Note that this is NOT a relocatable package
%define ver      0.3.0
%define rel      SNAP
%define prefix   /usr

Summary:   Miscellaneous Memory and Configuration Function Library
Name:      stringlist
Version:   %ver
Release:   %rel
Copyright: GPL
Group:     X11/Libraries
Source0:   stringlist-%{PACKAGE_VERSION}.tar.gz
URL:       http://mandrake.net/
BuildRoot: /tmp/stringlist-%{PACKAGE_VERSION}-root
Packager: Term <term@dfw.net>

%description
The StringList library provides several miscellaneous sets of functions
for tracking memory usage, Enlightenment's configuration language, file
and string manipulations.

%package devel
Summary: Libraries and includes necessary for building other applications.
Group: X11/Libraries
Requires: stringlist

%description devel
Libraries and includes required for building other applications with
StringList.
 
%prep
%setup

%build
./autogen.sh --prefix=/usr
# Needed for snapshot releases.
if [ ! -f configure ]; then
  CFLAGS="$RPM_OPT_FLAGS" ./autogen.sh --prefix=%prefix
else
  CFLAGS="$RPM_OPT_FLAGS" ./configure --prefix=%prefix
fi

if [ "$SMP" != "" ]; then
  (make "MAKE=make -k -j $SMP"; exit 0)
  make
else
  make
fi

%install
rm -rf $RPM_BUILD_ROOT

make prefix=$RPM_BUILD_ROOT%{prefix} install

%post
/sbin/ldconfig

%postun -p /sbin/ldconfig

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-, root, root)
%doc AUTHORS COPYING ChangeLog INSTALL NEWS README
%{prefix}/lib/lib*.so.*

%files devel
%defattr(-, root, root)

%{prefix}/lib/lib*.so
%{prefix}/lib/*a
%{prefix}/include/*


%changelog
* Tue Aug 18 1998 Term <term@dfw.net>

- First try at an RPM for StringList, hacked from esound's. :)
